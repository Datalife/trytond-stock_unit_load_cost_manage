# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.model import Unique
from trytond.modules.cost_manage.cost_manage import CostAllocationMixin


class CostAllocation(CostAllocationMixin, ModelSQL, ModelView):
    """Unit load cost allocation"""
    __name__ = 'cost.manage.unit_load.cost.allocation'
    _table = 'cm_unit_load_allocation'

    end_ = fields.Many2One('stock.unit_load', 'Cost end',
        ondelete='CASCADE', required=True, select=True)

    @classmethod
    def __setup__(cls):
        super(CostAllocation, cls).__setup__()
        cls._order = [
            ('date', 'ASC'),
            ('end_', 'ASC'),
            ('type_', 'ASC'),
            ('category', 'ASC')]
        t = cls.__table__()
        cls._sql_constraints = [
            ('uk_1', Unique(t, t.type_, t.source, t.end_, t.category),
                'stock_unit_load_cost_manage.'
                'msg_cost_manage_unit_load_cost_allocation_uk_1')]

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 3.6: reduce table name length
        old_table = 'cost_manage_unit_load_cost_allocation'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        super(CostAllocation, cls).__register__(module_name)
