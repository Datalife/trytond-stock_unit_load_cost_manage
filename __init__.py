# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .cost_manage import CostAllocation


def register():
    Pool.register(
        CostAllocation,
        module='stock_unit_load_cost_manage', type_='model')
